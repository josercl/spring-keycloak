# Ejemplo Spring Boot + Spring Security + Keycloak

## Keycloak

Con la imagen de docker de keycloak basta para correr este ejemplo

```shell
docker run -p 18080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:18.0.0 start-dev
```

Luego importar el realm "bitbase" usando el archivo bitbase-realm-export.json incluído en el repo

Eso creará el realm bitbase con 2 clientes ya preconfigurados:

* __bitbase-auth__: que se usará para generar los token jwt


* __bitbase-api__: Que será usado por spring boot / spring security para verificar la validez de los tokens, este cliente
no se usará para autenticación, solo para autorización
  
Para generar un token hay que crear primero un usuario en keycloak y asignarle algún rol

Ya al importar el json se crearon 3 roles: admin, role1 y role2

Para obtener un token basta con ejecutar:

```shell
curl --request POST \
  --url http://localhost:18080/realms/bitbase/protocol/openid-connect/token \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data client_id=bitbase-auth \
  --data username=<email del usuario> \
  --data password=<contraseña> \
  --data grant_type=password
```

Luego ese token tiene que enviarse en el header `Authorization` como bearer token

## Spring

```shell
./gradlew bootRun
```

El ejemplo corre en el puerto 8080

Hay 3 endpoints para probar:

GET /api/test_auth: Muestra información relacionada con el token, id del usuario en keycloak y sus roles

GET /api/test_role1: responde con http 200 si el usuario tiene rol "role1", http 403 de otra forma

GET /api/test_role2: responde con http 200 si el usuario tiene rol "role2", http 403 de otra forma
