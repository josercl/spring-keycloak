package com.example.keycloakexample.controller;

import org.keycloak.adapters.RefreshableKeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

@RestController
public class AuthController {

    @GetMapping("/api/test_token")
    Map<String, Object> testAuth(Principal principal) {
        KeycloakAuthenticationToken keycloakPrincipal = (KeycloakAuthenticationToken) principal;
        RefreshableKeycloakSecurityContext credentials = (RefreshableKeycloakSecurityContext) keycloakPrincipal.getCredentials();
        AccessToken token = credentials.getToken();

        Map<String, Object> result = new TreeMap<>();

        result.putAll(Map.of(
            "sesion_state", token.getSessionState(),
            "email_verified", token.getEmailVerified(),
            "name", token.getName(),
            "preferred_username", token.getPreferredUsername(),
            "given_name", token.getGivenName(),
            "family_name", token.getFamilyName(),
            "email", token.getEmail(),
            "realm_access", token.getRealmAccess()
        ));

        // Esto serían todos los claims extra que se definan en keycloak
        // en el ejemplo es phone
        result.putAll(token.getOtherClaims());

        return result;
    }

    @GetMapping("/api/test_role1")
    // Estos roles corresponden a los roles del usuario en keycloak
    @RolesAllowed({"role1", "admin"})
    String testRole1() {
        return "role role1 or role admin OK";
    }

    @GetMapping("/api/test_role2")
    @RolesAllowed("role2")
    String testRole2() {
        return "role role2 OK";
    }
}
